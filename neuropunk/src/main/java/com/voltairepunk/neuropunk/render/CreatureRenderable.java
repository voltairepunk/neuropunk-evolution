package com.voltairepunk.neuropunk.render;

import com.badlogic.gdx.graphics.Color;
import org.mini2Dx.core.engine.geom.CollisionPoint;
import org.mini2Dx.core.graphics.Graphics;

public class CreatureRenderable implements Renderable {

    private final CollisionPoint position;

    public CreatureRenderable(CollisionPoint position) {
        this.position = position;
    }

    @Override
    public void render(Graphics graphics) {
        graphics.setColor(Color.WHITE);
        graphics.fillCircle(position.x, position.y, 24);

        graphics.setColor(Color.GREEN);
        graphics.drawCircle(position.x, position.y - 8.f, 9);

        graphics.setColor(Color.BLACK);
        graphics.fillCircle(position.x, position.y - 13.f, 5);

    }
}
