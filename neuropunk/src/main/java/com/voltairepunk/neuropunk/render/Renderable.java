package com.voltairepunk.neuropunk.render;

import org.mini2Dx.core.graphics.Graphics;

public interface Renderable {

    void render(Graphics graphics);

}
