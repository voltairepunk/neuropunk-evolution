package com.voltairepunk.neuropunk.evoliution;

import com.voltairepunk.evolution.genetic.DNAFactoryInterface;

import java.util.Random;
import java.util.function.Supplier;

public class CreatureFactory implements DNAFactoryInterface<Creature, Byte> {

    @Override
    public Creature createDNA(int size, Random random, Supplier<Byte> getRandomGeneFunc, boolean shouldInitGenes) {
        return new Creature(size, random, getRandomGeneFunc, shouldInitGenes);
    }

    @Override
    public Creature createDNA(int size, Random random, Supplier<Byte> getRandomGeneFunc) {
        return new Creature(size, random, getRandomGeneFunc, true);
    }
}
