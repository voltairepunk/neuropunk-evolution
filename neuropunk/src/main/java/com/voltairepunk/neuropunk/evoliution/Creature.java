package com.voltairepunk.neuropunk.evoliution;

import com.voltairepunk.evolution.genetic.DNA;
import com.voltairepunk.neural.Network;
import com.voltairepunk.neuropunk.util.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

public class Creature extends DNA<Byte, Creature> {

    private static Random RANDOM = Util.RANDOM;

    private Network neuralNetwork;

    private double hunger = 0.0D;
    private double rotation = 0.0D;

    private int pelletsEaten = 0;

    public Creature(int size, Random random, Supplier<Byte> getRandomGeneFunc, boolean shouldInitGenes) {
        super(size, random, getRandomGeneFunc, shouldInitGenes);
    }

    @Override
    public double calculateFitness() {
        if (hunger == 0) {
            fitness = pelletsEaten;
        } else {
            fitness = (1 / hunger) + pelletsEaten;
        }
        return fitness;
    }

    @Override
    public Creature crossover(Creature mate) {
        Creature child = new Creature(getGenes().size(), getRandom(), getGetRandomGeneFunction(), false);

        List<Byte> parent1genes = getGenes();
        List<Byte> parent2genes = mate.getGenes();

        for (int i = 0; i < parent1genes.size(); i++) {
            Byte gene = mate.getRandom().nextDouble() < 0.5 ? parent1genes.get(i) : parent2genes.get(i);
            child.getGenes().add(gene);
        }

        return child;
    }

    @Override
    public Creature crossover(Creature mate, double mutationRate) {
        Creature child = crossover(mate);
        child.mutate(mutationRate);
        return child;
    }

    protected void setRandom(Random random) {
        this.random = random;
    }

    @Override
    public void birth() {
        List<Integer> topology = new ArrayList<>();
        topology.add(3);
        topology.add(6);
        topology.add(3);

        // Create weight topology
        List<List<Double>> weightTopology = new ArrayList<>();

        // We will not be looking through the last layer
        for (int i = 0; i < topology.size() - 1; i++) {
            int currNeurons = topology.get(i);
            int nextNeurons = topology.get(i + 1);

            List<Double> layerWeights = new ArrayList<>();
            for (int cn = 0; cn < currNeurons; cn++) {
                for (int nn = 0; nn < nextNeurons; nn++) {
                    int geneIndex = i + cn + nn;
                    double tempGene = (double) getGenes().get(geneIndex);
                    double value = (tempGene / Byte.MAX_VALUE) * 10.f;
                    layerWeights.add(value);
                }
            }
            weightTopology.add(layerWeights);
        }

        neuralNetwork = new Network(topology, weightTopology);

        rotation = 45.D * (Math.PI / 180.D);
    }

    public Network getNeuralNetwork() {
        return neuralNetwork;
    }

    public static byte getRandomGene() {
        byte[] b = new byte[1];
        RANDOM.nextBytes(b);

        return b[0];
    }

    private double[] foodInsight(Food food) {

//        double pelletDirection = Math.atan2((food.getPosition().y - position.y), (position.x - food.getPosition().x));
//        if (pelletDirection < 0) {
//            pelletDirection += (Math.PI * 2);
//        }
//
//        double distanceToFood = Math.abs(PVector.dist(position, food.getPosition()));
//
//        distanceToFood = distanceToFood / 1000.f;
//
//        double[] result = new double[2];
//        result[0] = pelletDirection;
//        result[1] = distanceToFood;
//        return result;

        return new double[2];
    }

    private double[] noFoodInSight() {
        double[] result = new double[2];
        result[0] = -1.f;
        result[1] = -1.f;
        return result;
    }

    private double[] updateGetFood(Food food) {
        if (food != null) {
            return foodInsight(food);
        }

        return noFoodInSight();
    }

    private double updateGetHunger() {
        hunger += 0.175f;

        if (hunger > 100.f) hunger = 100.f;

        return (hunger / 50.f) - 1.f;
    }

//    void update(Food food) {
//
//
//        List<Double> inputs = new ArrayList<>();
//        inputs.add(updateGetHunger());
//
//        double[] foodparams = updateGetFood(food);
//        inputs.add(foodparams[0]);
//        inputs.add(foodparams[1]);
//
//        neuralNetwork.feedForward(inputs);
//
//        List<Double> results = neuralNetwork.getResults();
//
//        double directionLeverage = results.get(0);
//        double movementLeverage = results.get(1);
//        double eatLeverage = results.get(2);
//
//        if (directionLeverage < -0.25f) {
//            rotation -= 7.0f * (Math.PI / 180);
//        }
//
//        if (directionLeverage > 0.25f) {
//            rotation += 7.0f * (Math.PI / 180);
//        }
//
//        if (Math.abs(rotation) > (2 * Math.PI)) {
//            rotation = 0.0f;
//        }
//
//        if (movementLeverage < -0.25 || movementLeverage > 0.25) {
//            double factor = 10.0f * (movementLeverage < 0 ? -1 : 1);
//            double angle = Math.PI / 2 - rotation;
//
//            double cosa = Math.cos(angle);
//            double cosb = Math.cos(rotation);
//
//            position.x += (factor * cosa);
//            position.y += ((-1) * factor * cosb);
//
//
//            if (position.x > 880) {
//                position.x = 880;
//            } else if (position.x < 0) {
//                position.x = 0;
//            }
//
//            if (position.y > 800) {
//                position.y = 800;
//            } else if (position.y < 0) {
//                position.y = 0;
//            }
//        }
//
//        if (eatLeverage > 0 && food != null) {
//            if (food.isAlive()) {
//                hunger = 0;
//                pelletsEaten++;
//                food.destroy();
//            }
//        }
//
//    }
//
//    PVector getPosition() {
//        return position;
//    }
//
//    void setPosition(PVector value) {
//        position = value;
//    }

    public void display() {


        // Temporary
//        if (networkRender == null) {
//            networkRender = new NetworkRenderImpl(sketch, view, sketch.createFont("Ubuntu", 32, true));
//        }

        // Draw the creature
//        view.pushMatrix();
//        view.fill(255);
//        view.strokeWeight(2);
//        view.stroke(0);
//        view.translate(position.x, position.y);
//        view.rotate((float) rotation);
//        float bodyRadius = 24.f;
//        view.ellipse(0, 0, bodyRadius, bodyRadius);
//        view.fill(0, 0, 255);
//        float faceRadius = 8.f;
//        view.ellipse(0, -10.f, faceRadius, faceRadius);
//        view.popMatrix();

        // Draw the neural network
//        try {
//            networkRender.render(neuralNetwork);
//        } catch (TypeMismatchException e) {
//            e.printStackTrace();
//        }
    }
}
