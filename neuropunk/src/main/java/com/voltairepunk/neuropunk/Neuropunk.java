package com.voltairepunk.neuropunk;

import com.badlogic.gdx.backends.lwjgl.DesktopMini2DxGame;
import com.voltairepunk.neuropunk.evoliution.Creature;
import com.voltairepunk.neuropunk.render.CreatureRenderable;
import org.mini2Dx.core.engine.geom.CollisionPoint;
import org.mini2Dx.core.game.BasicGame;
import org.mini2Dx.core.graphics.Graphics;
import org.mini2Dx.desktop.DesktopMini2DxConfig;

public class Neuropunk  {

    private final static int DEFAULT_SCREEN_WIDTH = 1280;
    private final static int DEFAULT_SCREEN_HEIGHT = 800;

    private static class Overwatch extends BasicGame {

        public static final String GAME_IDENTIFIER = "com.basic.game.example";

        private CreatureRenderable testCreature = new CreatureRenderable(new CollisionPoint(300, 300));

        @Override
        public void initialise() {

        }

        @Override
        public void update(float v) {

        }

        @Override
        public void interpolate(float v) {

        }

        @Override
        public void render(Graphics graphics) {
            graphics.drawRect(32, 32, 64, 64);
            testCreature.render(graphics);
        }
    }

    Creature creature;

//    private PGraphics view;

    public static void main(String... args) {

        DesktopMini2DxConfig cfg = new DesktopMini2DxConfig(Overwatch.GAME_IDENTIFIER);
        cfg.title = "Basic Game Example";
        cfg.width = 800;
        cfg.height = 600;
        cfg.vSyncEnabled = true;
        new DesktopMini2DxGame(new Overwatch(), cfg);

//        PApplet.runSketch(processingArgs, new Neuropunk());
//        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
//        config.title = "Drop";
//        config.width = 800;
//        config.height = 480;
//        new LwjglApplication(new ApplicationAdapter() {
//            @Override
//            public void create() {
//                super.create();
//            }
//        }, config);
    }

//    @Override
//    public void settings(){
//        size(DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT, P2D);
//    }
//
//    @Override
//    public void setup() {
//
////        leftView = createGraphics(400, 800, P2D);
////        rightView = createGraphics(880, 800, P2D);
////
////        world = new World(10, 36, Util.RANDOM, Creature::getRandomGene, 2, 0.015f);
////        world.setSketch(this);
////        world.initPopulation();
////
////        timer = new Timer();
////        updaterTask = new UpdaterTask(world);
////        timer.schedule(updaterTask, 0, 10);
//
//        view = createGraphics(DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT, P2D);
//
//        creature = new Creature(
//                36, Util.RANDOM, Creature::getRandomGene, true
//                );
//
//        creature.setRenderable(() -> {
//            view.pushMatrix();
//            view.fill(255);
//            view.strokeWeight(2);
//            view.stroke(0);
//            view.translate(200, 200);
//            view.rotate((float) 0);
//            float bodyRadius = 50.f;
//            view.ellipse(0, 0, bodyRadius, bodyRadius);
//            view.fill(0, 0, 255);
//            float faceRadius = 16.f;
//            view.ellipse(0, -30.f, faceRadius, faceRadius);
//            view.popMatrix();
//        });
//
//        frameRate(60);
//    }
//
//    @Override
//    public void draw() {
//        background(125);
//        creature.display();
//    }



}
