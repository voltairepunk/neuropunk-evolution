package com.voltaire.neural;

public interface RenderableInterface {

    <T> void render(T item) throws TypeMismatchException;

}
