package com.voltaire.neural;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;

public abstract class ProcessingRenderer implements RenderableInterface {

    protected PApplet sketch;
    protected PGraphics view;
    protected PFont font;

    public ProcessingRenderer(PApplet sketch, PGraphics view, PFont font) {
        this.sketch = sketch;
        this.view = view;
        this.font = font;
    }
}
