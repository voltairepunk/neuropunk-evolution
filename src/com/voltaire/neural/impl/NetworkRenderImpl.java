package com.voltaire.neural.impl;

import com.voltaire.neural.*;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;

public class NetworkRenderImpl extends ProcessingRenderer {

    ConnectionRenderImpl connectionRender;
    PerceptronRenderImpl perceptronRender;

    public NetworkRenderImpl(PApplet sketch, PGraphics view, PFont font) {
        super(sketch, view, font);
        connectionRender = new ConnectionRenderImpl(sketch, view, font);
        perceptronRender = new PerceptronRenderImpl(sketch, view, font);
    }

    @Override
    public <T> void render(T item) throws TypeMismatchException {
        if (!(item instanceof Network)) {
            throw new TypeMismatchException("Not a neural network...");
        }

        Network network = (Network) item;

        for (Connection conn : network.getConnections()) {
            connectionRender.render(conn);
        }

        for (Perceptron per : network.getNeurons()) {
            perceptronRender.render(per);
        }
    }
}
