package com.voltaire.neural.impl;

import com.voltaire.neural.Connection;
import com.voltaire.neural.Perceptron;
import com.voltaire.neural.ProcessingRenderer;
import com.voltaire.neural.TypeMismatchException;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;
import processing.core.PVector;

public class ConnectionRenderImpl extends ProcessingRenderer {

    public ConnectionRenderImpl(PApplet sketch, PGraphics view, PFont font) {
        super(sketch, view, font);
    }

    @Override
    public <T> void render(T item) throws TypeMismatchException {
        if (!(item instanceof Connection)) {
            throw new TypeMismatchException("Not a connection...");
        }

        Connection conn = (Connection) item;

        if (conn.getWeight() < 0) {
            view.stroke(255,0,0);
        } else if (conn.getWeight() > 0) {
            view.stroke(0,255,0);
        } else {
            view.stroke(0,0,0);
        }

        view.strokeWeight(1 + PApplet.abs(conn.getWeight() * .05f));
        Perceptron from = conn.getFrom();
        Perceptron to = conn.getTo();

        view.line(from.getPosition().x, from.getPosition().y, to.getPosition().x, to.getPosition().y);

        PVector midpoint = new PVector((from.getPosition().x + to.getPosition().x) / 2,
                (from.getPosition().y + to.getPosition().y) / 2);
        view.fill(0);
        view.textFont(font, 16);
        view.textAlign(PApplet.CENTER, PApplet.CENTER);
        view.text(conn.getWeight(), midpoint.x, midpoint.y);
    }
}
