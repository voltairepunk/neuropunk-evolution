package com.voltaire.neural.impl;

import com.voltaire.neural.Perceptron;
import com.voltaire.neural.ProcessingRenderer;
import com.voltaire.neural.RenderableInterface;
import com.voltaire.neural.TypeMismatchException;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;

public class PerceptronRenderImpl extends ProcessingRenderer {


    public PerceptronRenderImpl(PApplet sketch, PGraphics view, PFont font) {
        super(sketch, view, font);
    }

    @Override
    public <T> void render(T item) throws TypeMismatchException {
        if (!(item instanceof Perceptron)) {
            throw new TypeMismatchException("Not a perceptron...");
        }

        Perceptron perceptron = (Perceptron) item;

        view.stroke(0);
        view.strokeWeight(1);
        float b = PApplet.map(perceptron.getOutput(), 0, 1, 255, 0);
        view.fill(b);
        view.ellipse(perceptron.getPosition().x, perceptron.getPosition().y, 32.f, 32.f);

        b = PApplet.map(perceptron.getOutput(), 0, 1, 0, 255);
        view.fill(b);
        view.textFont(font, 16);
        view.textAlign(PApplet.CENTER, PApplet.CENTER);
        view.text(perceptron.getOutput(), perceptron.getPosition().x, perceptron.getPosition().y);
    }
}
