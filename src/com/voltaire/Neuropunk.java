package com.voltaire;

import com.voltaire.evolution.Creature;
import com.voltaire.evolution.World;
import com.voltaire.util.UpdaterTask;
import com.voltaire.util.Util;
import processing.core.PApplet;
import processing.core.PGraphics;

import java.util.Timer;
import java.util.TimerTask;

public class Neuropunk extends PApplet {

    private static int DEFAULT_SCREEN_WIDTH = 1280;
    private static int DEFAULT_SCREEN_HEIGHT = 800;

    private PGraphics leftView;
    private PGraphics rightView;

    World world;

    Timer timer;
    TimerTask updaterTask;

    public static void main(String... args){
        String[] processingArgs = {"MySketch"};
        Neuropunk mySketch = new Neuropunk();
        PApplet.runSketch(processingArgs, mySketch);
    }

    @Override
    public void settings(){
        size(DEFAULT_SCREEN_WIDTH, DEFAULT_SCREEN_HEIGHT, P2D);
    }

    @Override
    public void setup() {

        leftView = createGraphics(400, 800, P2D);
        rightView = createGraphics(880, 800, P2D);

        world = new World(10, 36, Util.RANDOM, Creature::getRandomGene, 2, 0.015f);
        world.setSketch(this);
        world.initPopulation();

        timer = new Timer();
        updaterTask = new UpdaterTask(world);
        timer.schedule(updaterTask, 0, 10);

        frameRate(60);
    }

    @Override
    public void draw(){
        background(125);

        if (world.shouldDisplay()) {
            world.display(leftView, rightView);
        }

        image(leftView, 0, 0);
        image(rightView, 400, 0);
        stroke(125);
        strokeWeight(8.f);
        line(400, 0, 400, height);

    }

    @Override
    public void mouseClicked() {
        System.out.println("CLICK");
        world.triggerClick();
    }

    @Override
    public void mouseReleased() {
        System.out.println("UNCLICK");
        world.triggerRelease();
    }
}
