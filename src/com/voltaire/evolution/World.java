package com.voltairepunk.neuropunk.evoliution;


import com.voltaire.genetic.Algorithm;
import com.voltaire.ui.Button;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;
import processing.core.PVector;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

public class World extends Algorithm<Creature, Byte> {

    CreatureFactory creatureFactory;
    PApplet sketch;

    List<Food> foodList;
    private final Object mutex;

    private PFont font;
    Button newGenerationButton;
    boolean spawnNewGeneration;
    boolean shouldDisplay;


    public World(int populationSize, int dnaSize, Random random, Supplier<Byte> getRandomGeneFunc, int elitism, float mutationRate) {
        super(populationSize, dnaSize, random, getRandomGeneFunc, elitism, mutationRate);
        mutex = new Object();
        creatureFactory = new CreatureFactory();
        setDnaFactory(creatureFactory);
        spawnNewGeneration = false;
        shouldDisplay = true;

        foodList = new ArrayList<>();
    }

    public void initPopulation() {
        super.initPopulation();

        synchronized (mutex) {
            newGenerationButton = new Button(sketch, "New Generation", new PVector(80, 620), new PVector(200, 40));

            for (Creature creature : population) {
                creature.setSketch(sketch);
                PVector randomPosition = generateRandomPosition();
                creature.setPosition(randomPosition);
                creature.birth();
            }

            generateRandomFood(population.size() / 2);
        }
    }

    private void generateRandomFood(int count) {

        if (!foodList.isEmpty()) {
            for (Food food : foodList) {
                food.destroy();
            }

            foodList.clear();
        }

        // Generate some food
        for (int i = 0; i < count; i++) {
            PVector randomFoodPos = generateRandomPosition();
            Food food = new Food(sketch, randomFoodPos);
            foodList.add(food);
        }
    }

    private PVector generateRandomPosition() {
        float xpos = sketch.random(0, 800);
        float ypos = sketch.random(0, 800);

        return new PVector(xpos, ypos);
    }

    public void update() {
        synchronized (mutex) {
            for (Creature creature : population) {
                creature.update(getClosestFood(creature));
            }


            foodList.removeIf(food -> !food.isAlive());
            calculateFitness();

            newGenerationButton.update();

            if (newGenerationButton.isDown() && spawnNewGeneration) {
                newGeneration(true);
                generateRandomFood(population.size() / 2);
                spawnNewGeneration = false;
                shouldDisplay = true;
            }
        }
    }

    private Food getClosestFood(Creature creature) {

        Food closest = null;
        PVector cpos = creature.getPosition();

        for (Food food : foodList) {
            PVector fpos = food.getPosition();

            if ((PApplet.abs(fpos.dist(cpos)) < 30.f)) {
                closest = food;
                return closest;
            }
        }

        return closest;
    }

    public void display(PGraphics leftView, PGraphics rightView) {

        if (font == null) {
            // TODO: Move this to a seperate setup function or pseudo initializer
            font = this.sketch.createFont("Ubuntu", 16, true);
        }
        synchronized (mutex) {

            leftView.beginDraw();
            leftView.background(125);

            leftView.fill(255);
            leftView.textFont(font, 12);
            leftView.textAlign(sketch.LEFT, sketch.CENTER);
            leftView.text("BEST FITNESS: " + getBestFitness(), 20, 20);
            leftView.text("GENERATION: " + getGeneration(), 20, 60);
            newGenerationButton.display(leftView);

            //network.display(leftView);
            leftView.endDraw();

            rightView.beginDraw();
            rightView.background(111);
            //testCreature.display(rightView);


            for (Food food : foodList) {
                food.display(rightView);
            }
            for (Creature creature : population) {
                creature.display(rightView);
            }

            rightView.endDraw();
        }

    }

    public void triggerClick() {
        synchronized (mutex) {
            newGenerationButton.triggerClick();
            spawnNewGeneration = newGenerationButton.isDown();
            shouldDisplay = !spawnNewGeneration;
        }
    }

    public void triggerRelease() {
        synchronized (mutex) {
            newGenerationButton.triggerRelease();
        }
    }

    public void setSketch(PApplet sketch) {
        this.sketch = sketch;
    }

    public boolean shouldDisplay() {
        synchronized (mutex) {
            return shouldDisplay;
        }
    }

}
