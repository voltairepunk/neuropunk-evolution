package com.voltaire;

import com.voltaire.genetic.Algorithm;
import com.voltaire.genetic.DNA;
import processing.core.PApplet;
import processing.core.PFont;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Random;

public class ShakesTest {

//    private static String TARGET_STRING = "To be, or not to be, that is the question.";
//    private static String VALID_CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,.|!#$%&/()=? ";
//    private int populationSize = 200;
//    private float mutationRate = 0.01f;
//    private int elitism = 100;
//
//    private PApplet sketch;
//    private PFont font;
//
//    private Algorithm<Character> ga;
//    private static Random RANDOM = new Random(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
//
//    public ShakesTest(PApplet sketch) {
//        this.sketch = sketch;
//        ga = new Algorithm<Character>(populationSize,
//                TARGET_STRING.length(), RANDOM,
//                ShakesTest::getRandomCharacter,
//                ShakesTest::fitnessFunction,
//                elitism, mutationRate);
//    }
//
//    public void update()
//    {
//        if (ga.getBestFitness() < 1) {
//            ga.newGeneration(true);
//        }
//    }
//
//    private static char getRandomCharacter() {
//        int i = RANDOM.nextInt(VALID_CHARS.length());
//        return VALID_CHARS.charAt(i);
//    }
//
//    private static float fitnessFunction(DNA<Character> dna) {
//        float score = 0;
//        List<Character> genes = dna.getGenes();
//        for (int i = 0; i < genes.size(); i++) {
//            char gene = genes.get(i);
//            if (TARGET_STRING.charAt(i) == gene) {
//                score += 1;
//            }
//        }
//
//        score /= TARGET_STRING.length();
//        score = (float) (Math.pow(2, score) - 1);
//        return score;
//    }
//
//    public void setup() {
//        font = sketch.createFont("Ubuntu", 18, true);
//    }
//
//    public void display() {
//        float bestFitness = ga.getBestFitness();
//        int generation = ga.getGeneration();
//        List<Character> bestGenes = ga.getBestGenes();
//
//        sketch.textFont(font);
//        sketch.fill(255);
//        sketch.textAlign(sketch.LEFT, sketch.BOTTOM);
//
//        sketch.text("Best Fitness: " + bestFitness, 20, 40);
//        sketch.text("Generation: " + generation, 20, 80);
//        if (bestGenes.size() > 0) {
//            StringBuilder builder = new StringBuilder(bestGenes.size());
//            for (Character ch : bestGenes) {
//                builder.append(ch);
//            }
//            sketch.text("Best Genes: " + builder.toString(), 20, 120);
//        } else {
//            sketch.text("No best genes yet", 20, 120);
//        }
//    }

}
