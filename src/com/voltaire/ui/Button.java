package com.voltaire.ui;

import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PGraphics;
import processing.core.PVector;

public class Button {

    private PApplet sketch;

    private final String label;
    private final int textColor;
    private final int backColor;
    private final int borderColor;

    private final int hoverColor;
    private final int downColor;

    private final PVector position;
    private final PVector dimmensions;

    private boolean isHover = false;
    private boolean isDown = false;

    PFont font;

    public Button(PApplet sketch, String label, PVector pos, PVector dim) {
        this.sketch = sketch;
        this.label = label;
        textColor = sketch.color(255);
        backColor = sketch.color(0, 200, 0);
        borderColor = sketch.color(0, 100, 0);
        position = pos;
        dimmensions = dim;
        font = sketch.createFont("Ubuntu", 32, true);

        hoverColor = sketch.color(0, 0, 200);
        downColor = sketch.color(200, 0, 0);
    }

    public void display(PGraphics view) {
        view.stroke(borderColor);
        if (isHover) {
            view.fill(hoverColor);
        } else if (isDown) {
            view.fill(downColor);
        } else {
            view.fill(backColor);
        }
        view.rect(position.x, position.y, dimmensions.x, dimmensions.y);

        view.fill(textColor);
        view.textFont(font, 16);
        view.textAlign(sketch.CENTER, sketch.CENTER);
        view.text(label, position.x + (dimmensions.x / 2), position.y + (dimmensions.y / 2));
    }

    public void update() {
        checkHover();
    }

    private void checkHover() {
        isHover = sketch.mouseX > position.x & sketch.mouseX < position.x + dimmensions.x & sketch.mouseY > position.y & sketch.mouseY < position.y + dimmensions.y;
        if (!isHover) {
            isDown = false;
        }
    }

    public void triggerClick() {
        isDown = isHover;
        isHover = false;
    }

    public void triggerRelease() {
        isDown = false;
    }

    public boolean isDown() { return isDown; }

}
