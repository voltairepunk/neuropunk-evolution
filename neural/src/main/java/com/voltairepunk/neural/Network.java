package com.voltairepunk.neural;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class Network {

    private double recentAverageError;
    private double recentAverageSmoothingFactor;
    private double error;

    // All known neurons arranged in layers
    private List<List<Perceptron>> layers;
    // All known inputs for convience
    private List<Perceptron> inputs;
    // All known neurons
    private List<Perceptron> neurons;
    // All known connections
    private List<Connection> connections;

    public double getError() {
        return error;
    }

    public Network(List<Integer> topology, List<List<Double>> weightTopology) {
        layers = new ArrayList<>();
        neurons = new ArrayList<>();
        inputs = new ArrayList<>();
        connections = new ArrayList<>();

        topology.forEach(layerNumber -> {
                    List<Perceptron> layer = new ArrayList<>();
                    for (int i = 0; i < layerNumber; i++) {
                        Perceptron neuron = new Neuron();
                        layer.add(neuron);
                        if (layers.size() == 0) {
                            // Input layer
                            inputs.add(neuron);
                        }
                        neurons.add(neuron);
                    }
                    layers.add(layer);
                });

        // Create the connections between the neurons
        for (int lNum = 0; lNum < layers.size(); lNum++) {
            // No connections are needed for the last layer
            if (lNum != (layers.size() - 1)) {
                List<Perceptron> curr = layers.get(lNum);
                List<Perceptron> next = layers.get(lNum + 1);

                for (int i = 0; i < curr.size(); i++ )  {
                    Neuron from = (Neuron) curr.get(i);
                    for (int j = 0; j < next.size(); j++) {
                        Neuron to = (Neuron) next.get(j);
                        Connection conn = new Connection(from, to, weightTopology.get(lNum).get(i + j));
                        from.addOutputConnection(conn);
                        to.addInputConnection(conn);
                        connections.add(conn);
                    }
                }
            }
        }
    }

    public void feedForward(List<Double> in) {
        if (inputs.size() != in.size()) {
            throw new IllegalArgumentException("Input values should match input errors");
        }

        // Assign input values to input neurons
        for (int i = 0; i < in.size(); i++) {
            inputs.get(i).setOutput(in.get(i));
        }

        // Feed forward the network
        for (int lNum = 1; lNum < layers.size(); lNum++) {
            List<Perceptron> layer = layers.get(lNum);
            for (Perceptron neuron : layer) {
                neuron.feedForward();
            }
        }
    }

    public void backPropagation(List<Float> targets) {
        // Calculate overall net error (RMS of oputput neuron errors)
        List<Perceptron> outputLayer = layers.get(layers.size() - 1);
        error = 0.0f;

        // Don't forget to calculate output gradients
        for (int n = 0; n < outputLayer.size(); n++) {
            outputLayer.get(n).calcOutputGradients(targets.get(n));
            double delta = targets.get(n) - outputLayer.get(n).getOutput();
            error += (delta * delta);
        }

        error = error / outputLayer.size();
        error = Math.sqrt(error);

        // Recent average measurement
        recentAverageError = (recentAverageError * recentAverageSmoothingFactor + error) /
                (recentAverageSmoothingFactor + 1.0D);

        // Calculate gradients on hidden layers
        for (int lNum = layers.size() - 2; lNum > 0; lNum--) {
            List<Perceptron> hiddenLayer = layers.get(lNum);
            for (Perceptron per : hiddenLayer) {
                per.calcHiddenGradients();
            }
        }

        // update the connection weights
        for (int lNum = layers.size() - 1; lNum > 0; lNum--) {
            List<Perceptron> layer = layers.get(lNum);
            for (Perceptron per : layer) {
                per.updateInputWeights();
            }
        }
    }

    public List<Double> getResults() {
        List<Double> results = new ArrayList<>();
        List<Perceptron> outputLayer = layers.get(layers.size() - 1);
        for (Perceptron per : outputLayer) {
            results.add(per.getOutput());
        }
        return results;
    }

    public List<Perceptron> getNeurons() { return neurons; }

    public List<Connection> getConnections() { return connections; }
}
