package com.voltairepunk.neural;

public abstract class Perceptron {

    // Output
    private double output;
    private double gradient;

    Perceptron() {
        output = 0.0D;
        gradient = 0.0D;
    }

    public void setOutput(double value) {
        output = value;
    }

    public double getOutput() {
        return output;
    }

    public void setGradient(double value) {
        gradient = value;
    }

    public double getGradient() {
        return gradient;
    }


    abstract double activation(double value);
    abstract double activationDeviation(double value);
    abstract void feedForward();
    abstract void calcOutputGradients(double target);
    abstract void calcHiddenGradients();
    abstract double sumDOW();
    abstract void updateInputWeights();

}
