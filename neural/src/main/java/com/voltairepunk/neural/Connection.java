package com.voltairepunk.neural;

public class Connection {

    private Perceptron from;
    private Perceptron to;

    private double weight;
    private double deltaWeight;


    public Connection(Neuron from, Neuron to) {
        this.from = from;
        this.to = to;
        weight = 0;
    }

    public Connection(Neuron from, Neuron to, double weight) {
        this(from, to);
        this.weight = weight;
    }

    public void setWeight(double value) { weight = value; }

    public void setDeltaWeight(double value) { deltaWeight = value; }

    public double getWeight() { return weight; }

    public double getDeltaWeight() { return deltaWeight; }

    public Perceptron getFrom() { return from; }

    public Perceptron getTo() { return to; }
}
