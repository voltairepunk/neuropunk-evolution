package com.voltairepunk.neural;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Neuron extends Perceptron {

    private double eta = 0.15f;
    private double alpha = 0.15f;

    // Connections
    private List<Connection> inputc;
    private List<Connection> outputc;

    public Neuron() {
        inputc = new ArrayList<>();
        outputc = new ArrayList<>();
    }

    public void addInputConnection(Connection c) {
        inputc.add(c);
    }

    public void addOutputConnection(Connection c) {
        outputc.add(c);
    }

    @Override
    double activation(double value) {
        return (1 - Math.exp(0 - value)) / (1 + Math.exp(0 - value));
    }

    @Override
    double activationDeviation(double value) {
        return 1 - (value * value);
    }

    @Override
    public void feedForward() {
        double sum = inputc.stream()
                .mapToDouble(connection -> (connection.getFrom().getOutput() * connection.getWeight()))
                .sum();

        setOutput(activation(sum));
    }

    @Override
    void calcOutputGradients(double target) {
        double delta = target - getOutput();
        setGradient(delta * activationDeviation(getOutput()));
    }

    @Override
    void calcHiddenGradients() {
        double dow = sumDOW();
        setGradient(dow * activationDeviation(getOutput()));
    }

    @Override
    double sumDOW() {
        return inputc.stream()
                .mapToDouble(connection -> (connection.getWeight() * connection.getTo().getGradient()))
                .sum();
    }

    @Override
    public void updateInputWeights() {
        inputc.forEach(connection -> {
            Neuron neuron = (Neuron) connection.getFrom();
            double oldDeltaW = connection.getDeltaWeight();
            double newDeltaW =
                    eta
                            * neuron.getOutput()
                            * getGradient()
                            + alpha
                            * oldDeltaW;

            connection.setDeltaWeight(newDeltaW);
            connection.setWeight(connection.getWeight() + newDeltaW);
        });
    }
}
