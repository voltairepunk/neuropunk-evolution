package com.voltairepunk.evolution.genetic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

public abstract class DNA<T, V extends DNA> {

    private List<T> genes;
    protected double fitness;

    protected Random random;
    private Supplier<T> getRandomGene;

    protected DNA(int size,
                Random random,
                Supplier<T> getRandomGeneFunc,
                boolean shouldInitGenes) {
        genes = new ArrayList<>();
        this.random = random;
        this.getRandomGene = getRandomGeneFunc;

        if (shouldInitGenes) {
            for (int i = 0; i < size; i++) {
                genes.add(getRandomGene.get());
            }
        }
    }

    public DNA(int size,
               Random random,
               Supplier<T> getRandomGeneFunc) {
        this(size, random, getRandomGeneFunc, true);
    }

    public abstract double calculateFitness();

    public abstract V crossover(V mate);

    public abstract V crossover(V mate, double mutationRate);

    public abstract void birth();

    public void mutate(double mutationRate) {
        for (int i = 0; i < genes.size(); i++) {
            if (random.nextDouble() < mutationRate) {
                genes.set(i, getRandomGene.get());
            }
        }
    }

    public List<T> getGenes() { return genes; }

    public double getFitness() { return fitness; }

    public Random getRandom() { return random; }

    public Supplier<T> getGetRandomGeneFunction() { return getRandomGene; }
}
