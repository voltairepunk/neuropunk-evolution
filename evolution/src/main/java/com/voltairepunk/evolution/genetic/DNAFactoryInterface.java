package com.voltairepunk.evolution.genetic;

import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;

public interface DNAFactoryInterface<T extends DNA<V, T>, V> {

    T createDNA(int size, Random random, Supplier<V> getRandomGeneFunc, boolean shouldInitGenes);
    T createDNA(int size, Random random, Supplier<V> getRandomGeneFunc);


}
