package com.voltairepunk.evolution.genetic;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

public class Algorithm<T extends DNA<V, T>, V> {

    protected List<T> population;
    private int generation;
    private double bestFitness;
    private List<V> bestGenes;

    private int elitism;
    private double mutationRate;
    private int populationSize;

    protected List<T> newPopulation;
    private Random random;
    private double fitnessSum;
    private int dnaSize;
    private Supplier<V> getRandomGene;

    private DNAFactoryInterface<T, V> dnaFactory;

    private final Object mutex;

    public Algorithm(int populationSize,
                     int dnaSize,
                     Random random,
                     Supplier<V> getRandomGeneFunc,
                     int elitism,
                     double mutationRate) {
        this.populationSize = populationSize;
        generation = 1;
        this.elitism = elitism;
        this.mutationRate = mutationRate;
        population = new ArrayList<>(populationSize);
        newPopulation = new ArrayList<>(populationSize);
        this.random = random;
        this.dnaSize = dnaSize;
        this.getRandomGene = getRandomGeneFunc;
        bestGenes = new ArrayList<>(dnaSize);
        mutex = new Object();
    }

    protected void initPopulation() {
        if (dnaFactory == null) {
            throw new NullPointerException("DNA Factory not set");
        }
        for (int i = 0; i < populationSize; i++) {
            T individual = dnaFactory.createDNA(dnaSize, random, getRandomGene);
            population.add(individual);
        }
    }

    public void setDnaFactory(DNAFactoryInterface<T, V> factory) {
        dnaFactory = factory;
    }

    public double getBestFitness() {
        synchronized (mutex) {
            return bestFitness;
        }
    }

    public int getGeneration() {
        return generation;
    }

    public List<V> getBestGenes() {
        synchronized (mutex) {
            return bestGenes;
        }
    }

    public void newGeneration(boolean crossover) {
        if (population.size() <= 0) return;

        calculateFitness();
        population.sort(this::compareDNA);
        newPopulation.clear();

        for (int i = 0; i < population.size(); i++) {
            if (i < elitism) {
                newPopulation.add(population.get(i));
            } else if (crossover) {

                try {
                    T parent1 = chooseParent();
                    T parent2 = chooseParent();

                    T child = parent1.crossover(parent2);
                    child.birth();

                    newPopulation.add(child);
                } catch (NullPointerException e) {
                    // Void
                }
            } else {
                T individual = dnaFactory.createDNA(dnaSize, random, getRandomGene, true);
                individual.birth();
                newPopulation.add(individual);
            }
        }

        int missing = population.size() - newPopulation.size();
        for (int i = 0; i < missing; i++) {
            T individual = dnaFactory.createDNA(dnaSize, random, getRandomGene, true);
            individual.birth();
            newPopulation.add(individual);
        }

        List<T> temp = population;
        population = newPopulation;
        newPopulation = temp;

        generation++;

    }

    private int compareDNA(T a, T b) {
        if (a.getFitness() > b.getFitness()) {
            return -1;
        } else if (a.getFitness() < b.getFitness()) {
            return 1;
        }

        return 0;
    }

    protected void calculateFitness() {
        fitnessSum = 0;

        synchronized (mutex) {
            T best = population.get(0);
            for (T dna : population) {
                fitnessSum += dna.calculateFitness();

                if (dna.getFitness() > best.getFitness()) {
                    best = dna;
                }
            }

            bestFitness = best.getFitness();
            bestGenes = new ArrayList<>(best.getGenes());
        }
    }

    private T chooseParent() throws NullPointerException {
        double randyChooser = random.nextDouble() * fitnessSum;

        for (T dna : population) {
            if (randyChooser < dna.getFitness()) {
                return dna;
            }
            randyChooser -= dna.getFitness();
        }

        throw new NullPointerException("Could not find parent");
    }


}
